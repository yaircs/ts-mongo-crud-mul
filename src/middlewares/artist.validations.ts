import { Request, Response, NextFunction } from 'express';
import { createOrPutSchemaBody, patchSchemaBody, addSongToArtistSchemaParams } from '../modules/artist/artist.validation.schema.js';

export function artistValidateCreateOrPut(req: Request, res: Response, next: NextFunction) {
    createOrPutSchemaBody.validate(req.body).then((valid) => {
        next();
    }).catch(next);
}

export function artistValidatePatch(req: Request, res: Response, next: NextFunction) {
    patchSchemaBody.validate(req.body).then((valid) => {
        next();
    }).catch(next);
}

export function artistValidateAddSong(req: Request, res: Response, next: NextFunction) {
    addSongToArtistSchemaParams.validate(req.params.song_id).then((valid) => {
        next();
    }).catch(next);
}