import { Request, Response, NextFunction } from 'express';
import { createOrPutSchemaBody, patchSchemaBody, createOrPutSchemaParams, addOrRemoveSongSchemaBody } from '../modules/playlist/playlist.validation.schema.js';

export function playlistValidateCreateOrPutBody(req: Request, res: Response, next: NextFunction) {
    createOrPutSchemaBody.validate(req.body).then((valid) => {
        next();
    }).catch(next);
}

export function playlistValidateCreateOrPutParams(req: Request, res: Response, next: NextFunction) {
    createOrPutSchemaParams.validate(req.params).then((valid) => {
        next();
    }).catch(next);
}


export function playlistValidatePatchBody(req: Request, res: Response, next: NextFunction) {
    patchSchemaBody.validate(req.body).then((valid) => {
        next();
    }).catch(next);
}


export function playlistValidateSongsAddOrRemoveParams(req: Request, res: Response, next: NextFunction) {
    addOrRemoveSongSchemaBody.validate(req.params).then((valid) => {
        next();
    }).catch(next);
}