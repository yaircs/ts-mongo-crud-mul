import { Request, Response, NextFunction } from 'express';
import { createOrPutSchemaBody, patchSchema, createOrPutSchemaParams } from '../modules/song/song.validation.schema.js';

export function songValidateCreateOrPutBody(req: Request, res: Response, next: NextFunction) {
    createOrPutSchemaBody.validate(req.body).then((valid) => {
        next();
    }).catch(next);
}

export function songValidateCreateOrPutParams(req: Request, res: Response, next: NextFunction) {
    createOrPutSchemaParams.validate(req.params).then((valid) => {
        next();
    }).catch(next);
}

export function songValidatePatch(req: Request, res: Response, next: NextFunction) {
    patchSchema.validate(req.body).then((valid) => {
        next();
    }).catch(next);
}