import express from "express";
import morgan from "morgan";
import cors from "cors";
import fs from 'fs-extra';

import user_router from "../user/user.router.js";
import artist_router from "../artist/artist.router.js";
import song_router from "../song/song.router.js";
import playlist_router from "../playlist/playlist.router.js";

import { errorResponseMiddleware, logErrorMiddleware, printErrorMiddleware, notFoundMiddleware } from "../../middlewares/errors.handler.js";
import { logHttpMiddleware } from "../../middlewares/http.logger.js";

class App {

    public app: express.Application;

    constructor() {
        this.app = express();
        this.configGeneral();
        this.configMiddlewares();
        this.configRoutes();
        this.configErrorHandlers();
    }
    
    private configErrorHandlers() {
        const { ERRORS_LOG_PATH = './logs/users.errors.log' } = process.env;
        fs.ensureFileSync(ERRORS_LOG_PATH as string);
        this.app.use(logErrorMiddleware(ERRORS_LOG_PATH));
        this.app.use(printErrorMiddleware);
        this.app.use(errorResponseMiddleware);
        this.app.use(notFoundMiddleware);
    }
    private configGeneral() {
        this.app.use(cors());
        this.app.use(morgan("dev"));
        this.app.use(express.json());
    }
    private configRoutes() {
        this.app.use("/api/users", user_router);
        this.app.use("/api/artists", artist_router);
        this.app.use("/api/songs", song_router);
        this.app.use("/api/playlists", playlist_router);
    }
    private configMiddlewares() {
        const { HTTP_LOG_PATH = "./logs/users.http.log" } = process.env;
        fs.ensureFileSync(HTTP_LOG_PATH as string);
        this.app.use(logHttpMiddleware(HTTP_LOG_PATH));
    }
}
export default new App().app;