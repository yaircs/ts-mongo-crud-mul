import artist_model  from './artist.model.js';
import { ArtistData } from '../../typings/interfaces.js';
import artistRouter from './artist.router.js';

class MongoAdapter {
    async get_all_artists(): Promise<ArtistData[] | null> {
        const artists: ArtistData[] = await artist_model.find()
        .select('-__v -createdAt -updatedAt');
        
        return artists;
    }

    async create_artist(payload: ArtistData): Promise<ArtistData> {
        let newArtist: ArtistData = await artist_model.create(payload) as unknown as ArtistData;
        const { _id, name, song_ids } = newArtist;
        return { _id, name, song_ids };
    }

    async find_artist_by_id(artist_id: string): Promise<ArtistData | null> {
        const artist: ArtistData | null = await artist_model.findById(artist_id)
        .select('-__v -createdAt -updatedAt');
        
        return artist;
    }

    async find_artist_by_id_and_update(artist_id: string, payload: ArtistData): Promise<ArtistData | null> {
        const updatedArtist: ArtistData | null = await artist_model.findByIdAndUpdate(artist_id, payload, {new: true, upsert: false })
        .select('-__v -createdAt -updatedAt');
    
        return updatedArtist;
    }

    async find_artist_by_id_and_delete(artist_id: string): Promise<ArtistData | null> {
        const deletedArtist: ArtistData | null = await artist_model.findByIdAndDelete(artist_id)
        .select('-__v -createdAt -updatedAt');
    
        return deletedArtist;
    }

    async find_artist_by_id_and_add_song(artist_id: string, song_id: string): Promise<ArtistData | null> {
        const addedSongArtist = await artist_model.findById(artist_id)
        .select('-__v -createdAt -updatedAt');
        
        if(addedSongArtist){
            addedSongArtist.song_ids.push(song_id);
            await addedSongArtist.save();
        }

        return addedSongArtist;
        
    }

    async find_artist_by_id_and_remove_song(artist_id: string, song_id: string): Promise<ArtistData | null> {
        const removedSongArtist = await artist_model.findById(artist_id)
        .select('-__v -createdAt -updatedAt');

        const songIndex = removedSongArtist?.songs_ids.indexOf(song_id);
        if (songIndex > -1) {
            removedSongArtist.song_ids.splice(songIndex, 1);
            await removedSongArtist.save();
        }
        return removedSongArtist;
    }
}

const adapter = new MongoAdapter();
export default adapter;