
  import raw from "../../middlewares/route.async.wrapper.js";
  import * as artist_service from "./artist.services.js";
  import express, { Request, Response } from 'express';
  import { artistValidateCreateOrPut, artistValidateAddSong } from '../../middlewares/artist.validations.js';
//   import { UserData } from '../../typings/interfaces.js';
  
  
const artistRouter = express.Router();
artistRouter.use(express.json())

artistRouter.route('/')
    .get(raw( async (req: Request, res: Response): Promise<void> => {
        const artists = await artist_service.get_all_artists();
        res.status(200).json(artists);
    }))
    .post(artistValidateCreateOrPut, raw( async (req: Request, res: Response): Promise<void> => {
        let artist = await artist_service.create_artist(req.body);
        res.status(200).json(artist);
    }));


artistRouter.route('/:id')
    .get(raw( async (req: Request, res: Response): Promise<void> => {
        const artist = await artist_service.get_artist_by_id(req.params.id);
        if (!artist) res.status(404).json({ status: 400, message: `Artist ${req.params.id} not found.` });
        else res.status(200).json(artist);
    }))
    .put(artistValidateCreateOrPut, raw( async (req: Request, res: Response): Promise<void> => {
        let updatedArtist = await artist_service.find_artist_by_id_and_update(req.params.id, req.body);
        if (!updatedArtist) res.status(404).json({ status: 400, message: `Artist ${req.params.id} not found.` });
        else res.status(200).json(updatedArtist);
    }))
    .delete(raw( async (req: Request, res: Response): Promise<void> => {
        let deletedArtist = await artist_service.delete_artist_by_id(req.params.id);
        if (!deletedArtist) res.status(404).json({ status:400, message: `Artist ${req.params.id} not found.` });
        else res.status(200).json(deletedArtist);
    }));
 
    artistRouter.patch('/:id/add-song/:song_id', raw( async (req: Request, res: Response): Promise<void> => {
        let AddedSongArtist = await artist_service.find_artist_by_id_and_add_song(req.params.id ,req.params.song_id);
        res.status(200).json(AddedSongArtist);
    }));
  
  export default artistRouter;
  