import * as yup from 'yup';

export const createOrPutSchemaBody = yup.object().shape({
  name: yup.string().required().min(2).max(20),
  song_ids: yup.array()
});

export const patchSchemaBody = yup.object().shape({
  name: yup.string().required().min(2).max(20),
  song_ids: yup.array()
});

export const addSongToArtistSchemaParams = yup.object().shape({
  song_id: yup.string()
});