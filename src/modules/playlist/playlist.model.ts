import mongoose from 'mongoose';
const { Schema, model } = mongoose;

const PlaylistSchema = new Schema({
    name    : { type : String, required : true },
    user_id : { type: Schema.Types.ObjectId, ref:'user'},
    song_ids: [{ type: Schema.Types.ObjectId, ref:'song'}],
    genres  : [{ type : String }],
}, {timestamps: true});
  
export default model('playlist', PlaylistSchema);
