import playlist_model  from './playlist.model.js';
import { PlaylistData } from '../../typings/interfaces.js';
import moongose from 'mongoose';

class MongoAdapter {
    async get_all_playlists(): Promise<PlaylistData[]> {
        const playlists: PlaylistData[] = await playlist_model.find()
        .select('-__v -createdAt -updatedAt');

        return playlists;
    }

    async create_playlist(payload: PlaylistData): Promise<PlaylistData> {
        let newPlaylist: PlaylistData = await playlist_model.create(payload) as unknown as PlaylistData;
        const { _id, user_id, name, genres, song_ids } = newPlaylist;
        return { _id, user_id, name, genres, song_ids };
    }

    async find_playlist_by_id(playlist_id: string): Promise<PlaylistData | null> {
        const playlist: PlaylistData | null = await playlist_model.findById(playlist_id)
        .select('-__v -createdAt -updatedAt')
        .populate('song_ids', '-__v -createdAt -updatedAt');

        return playlist;
    }

    async find_playlist_by_id_and_update(playlist_id: string, payload: PlaylistData): Promise<PlaylistData | null> {
        const updatedPlaylist: PlaylistData | null = await playlist_model.findByIdAndUpdate(playlist_id, payload, {new: true, upsert: false })
        .select('-__v -createdAt -updatedAt');
        
        return updatedPlaylist;
    }

    async find_playlist_by_id_and_delete(playlist_id: string): Promise<PlaylistData | null> {
        const deletedPlaylist: PlaylistData | null = await playlist_model.findByIdAndDelete(playlist_id)
        .select('-__v -createdAt -updatedAt');
        return deletedPlaylist;
    }

    async find_playlist_by_id_and_add_song(playlist_id: string, song_id: string): Promise<PlaylistData | null> {
        const addedToPlaylist = await playlist_model.findById(playlist_id)
        .select('-__v -createdAt -updatedAt');

        const pushed = addedToPlaylist?.song_ids.push(song_id);
        pushed ? await addedToPlaylist.save() : null;
    
        return addedToPlaylist;
    }

    async find_playlist_by_id_and_remove_song(playlist_id: string, song_id: string): Promise<PlaylistData | null> {
        const removedFromPlaylist = await playlist_model.findById(playlist_id)
        .select('-__v -createdAt -updatedAt');

        const songIndex = removedFromPlaylist?.song_ids.indexOf(song_id);
        if (songIndex > -1) {
            removedFromPlaylist.song_ids.splice(songIndex, 1);
            await removedFromPlaylist.save();
        }
    
        return removedFromPlaylist;
    }

    // async find_playlist_by_id_and_remove_song(playlist_id: string, song_id: string): Promise<PlaylistData | null> {
    //     const removedFromPlaylist = await playlist_model.findById(playlist_id)
    //     .select('-__v -createdAt -updatedAt');

    //     removedFromPlaylist.song_ids = (<string[]>removedFromPlaylist.song_ids).filter(curr_song_id => curr_song_id !== song_id);
    //     await removedFromPlaylist.save();
    
    //     return removedFromPlaylist;
    // }

}

const adapter = new MongoAdapter();
export default adapter;