import { PlaylistData } from '../../typings/interfaces.js';
import adapter from './playlist.mongo.adapter.js';
import * as user_service from '../user/user.services.js';


// GET /api/playlists/
export const get_all_playlists = async (): Promise<PlaylistData[]> => {
    const playlists = await adapter.get_all_playlists();
    return playlists;
}

// POST /api/playlists/
export const create_playlist = async (payload: PlaylistData): Promise<PlaylistData> => {
    const newPlaylist = await adapter.create_playlist(payload);
    const owner_user = await user_service.get_user_by_id(newPlaylist.user_id);
    if(owner_user) {
        owner_user.playlist_ids.push(newPlaylist._id);
        await user_service.find_user_by_id_and_update(owner_user._id, owner_user);
    }
    return newPlaylist;
}

// GET /api/playlists/:id
export const get_playlist_by_id = async (playlist_id: string): Promise<PlaylistData | null> => {
    const playlist = await adapter.find_playlist_by_id(playlist_id);
    return playlist;
}

// PUT OR PATCH /api/playlists/:id
export const find_playlist_by_id_and_update = async (playlist_id: string, payload: PlaylistData): Promise<PlaylistData | null> => {
    const updatedPlaylist = await adapter.find_playlist_by_id_and_update(playlist_id, payload);
    return updatedPlaylist;
}

// DELETE /api/playlists/:id
export const delete_playlist_by_id = async (playlist_id: string): Promise<PlaylistData | null> => {
    const deletedPlaylist = await adapter.find_playlist_by_id_and_delete(playlist_id);
    if(deletedPlaylist){
        const owning_user = await user_service.get_user_by_id(deletedPlaylist.user_id);
        if(owning_user) {
            const playlistIndex = owning_user.playlist_ids.indexOf(playlist_id);
            if(playlistIndex > -1) {
                owning_user.playlist_ids.splice(playlistIndex, 1);
                await user_service.find_user_by_id_and_update(owning_user._id, owning_user);
            }
        }
    }
    return deletedPlaylist;
}

// PATCH /api/playlists/:id/add-song/:song_id
export const find_playlist_by_id_and_add_song = async (playlist_id: string, song_id: string): Promise< PlaylistData | null> => {
    const addedToPlaylist = await adapter.find_playlist_by_id_and_add_song(playlist_id, song_id);
    return addedToPlaylist;
}

// PATCH /api/playlists/:id/remove-song/:song_id
export const find_playlist_by_id_and_remove_song = async (playlist_id: string, song_id: string): Promise< PlaylistData | null> => {
    const addedToPlaylist = await adapter.find_playlist_by_id_and_remove_song(playlist_id, song_id);
    return addedToPlaylist;
}

// In use in song_service of DELETE /api/songs/:id/remove-song/:song_id
export const find_all_playlists_containing_song_id = async (song_id: string): Promise< PlaylistData[] | null> => {
    const allPlaylists = await adapter.get_all_playlists();
    let containingPlaylists = null;
    containingPlaylists = allPlaylists?.filter(playlist => playlist.song_ids.includes(song_id))
    return containingPlaylists;
}
