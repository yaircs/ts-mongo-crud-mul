import song_model  from './song.model.js';
import { SongData } from '../../typings/interfaces.js';

class MongoAdapter {
    async get_all_songs(): Promise<SongData[]> {
        const songs: SongData[] = await song_model.find()
        .select('-__v -createdAt -updatedAt');

        return songs;
    }

    async create_song(payload: SongData): Promise<SongData> {
        let newSong: SongData = await song_model.create(payload) as unknown as SongData;
        const { _id ,name, duration, artist_id, genre } = newSong;
        return { _id, name, duration, artist_id, genre };
    }

    async find_song_by_id(song_id: string): Promise<SongData | null> {
        const song: SongData | null = await song_model.findById(song_id)
        .select('-__v -createdAt -updatedAt');

        return song;
    }

    async find_song_by_id_and_update(song_id: string, payload: SongData): Promise<SongData | null> {
        const updatedSong: SongData | null = await song_model.findByIdAndUpdate(song_id, payload, {new: true, upsert: false })
        .select('-__v -createdAt -updatedAt');

        return updatedSong;
    }

    async find_song_by_id_and_delete(song_id: string): Promise<SongData | null> {
        const deletedSong: SongData | null = await song_model.findByIdAndDelete(song_id)
        .select('-__v -createdAt -updatedAt');
        
        return deletedSong;
    }
}

const adapter = new MongoAdapter();
export default adapter;