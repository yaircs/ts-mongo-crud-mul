import { SongData } from '../../typings/interfaces.js';
import adapter from './song.mongo.adapter.js';
import * as artist_service  from '../artist/artist.services.js';
import * as playlist_service  from '../playlist/playlist.services.js';


// GET /api/songs/
export const get_all_songs = async (): Promise<SongData[]> => {
    const songs = await adapter.get_all_songs();
    return songs;
}

// POST /api/songs/
export const create_song = async (payload: SongData): Promise<SongData> => {
    const newSong = await adapter.create_song(payload);
    // Also add song to artist's songs list
    await artist_service.find_artist_by_id_and_add_song(newSong.artist_id, newSong._id);
    return newSong;
}

// GET /api/songs/:id
export const get_song_by_id = async (song_id: string): Promise<SongData | null> => {
    const song = await adapter.find_song_by_id(song_id);
    return song;
}

// PUT OR PATCH /api/songs/:id
export const find_song_by_id_and_update = async (song_id: string, payload: SongData): Promise<SongData | null> => {
    const updatedSong = await adapter.find_song_by_id_and_update(song_id, payload);
    return updatedSong;
}

// // DELETE /api/songs/:id
// export const find_song_by_id_and_delete = async (song_id: string): Promise<SongData | null> => {
//     const deletedSong = await adapter.find_song_by_id_and_delete(song_id);
//     if(deletedSong) {
//         // Also remove song reference from artist's songs list
//         await artist_service.find_artist_by_id_and_remove_song(deletedSong.artist_id, song_id);
//         // and from all playlists containing this song
//         const playlistsToRemoveFrom = await playlist_service.find_all_playlists_containing_song_id(song_id);
//         playlistsToRemoveFrom?.forEach(async playlist => {
//             await playlist_service.find_playlist_by_id_and_remove_song(playlist._id, song_id);
//         });
//     }
    
//     return deletedSong;
// }

// DELETE /api/songs/:id
export const find_song_by_id_and_delete = async (song_id: string): Promise<SongData | null> => {
    const song = await adapter.find_song_by_id(song_id);
    if(song) {
        // Also remove song reference from artist's songs list
        await artist_service.find_artist_by_id_and_remove_song(song.artist_id, song_id);
        // and from all playlists containing this song
        const playlistsToRemoveFrom = await playlist_service.find_all_playlists_containing_song_id(song_id);
        playlistsToRemoveFrom?.forEach(async playlist => {
            await playlist_service.find_playlist_by_id_and_remove_song(playlist._id, song_id);
        });
    }
    const deletedSong = await adapter.find_song_by_id_and_delete(song_id);


    return deletedSong;
}
