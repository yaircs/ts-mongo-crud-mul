import * as yup from 'yup';

export const createOrPutSchemaBody = yup.object().shape({
  name: yup.string().required().min(2).max(20),
  duration: yup.string().required().min(4).max(10),
  genre: yup.string().min(3).max(10)
});

export const createOrPutSchemaParams = yup.object().shape({
  artist_id: yup.string().required()
});

export const patchSchema = yup.object().shape({
  name: yup.string().min(2).max(20),
  duration: yup.string().min(4).max(10),
  genre: yup.string().min(3).max(10)
}).test('at-least-one-field', "you must provide at least one field", value =>
!!(value.name  || value.duration || value.genre));