import user_model  from './user.model.js';
import { UserData } from '../../typings/interfaces.js';

class MongoAdapter {
    async get_all_users(): Promise<UserData[]> {
        const users: UserData[] = await user_model.find()
        .select('-__v -createdAt -updatedAt');

        return users;
    }

    async create_user(payload: UserData): Promise<UserData> {
        let newUser: UserData = await user_model.create(payload) as unknown as UserData;
        const { _id ,username, email, password, playlist_ids } = newUser;
        return { _id ,username, email, password, playlist_ids };
    }

    async find_user_by_id(user_id: string): Promise<UserData | null> {
        const user: UserData | null = await user_model.findById(user_id)
        .select('-__v -createdAt -updatedAt');

        return user;
    }

    async find_user_by_id_and_update(user_id: string, payload: UserData): Promise<UserData | null> {
        const updatedUser: UserData | null = await user_model.findByIdAndUpdate(user_id, payload, {new: true, upsert: false })
        .select('-__v -createdAt -updatedAt');

        return updatedUser;
    }

    async find_user_by_id_and_delete(user_id: string): Promise<UserData | null> {
        const deletedUser: UserData | null = await user_model.findByIdAndDelete(user_id)
        .select('-__v -createdAt -updatedAt');
        
        return deletedUser;
    }
}

const adapter = new MongoAdapter();
export default adapter;