import express, { Request, Response } from 'express';
import raw from "../../middlewares/route.async.wrapper.js";
import * as user_service from './user.services.js';
import { userValidateCreateOrPut, userValidatePatch } from '../../middlewares/user.validations.js';


const userRouter = express.Router();
userRouter.use(express.json());

userRouter.route('/')
    .get(raw( async (req: Request, res: Response): Promise<void> => {
        const users = await user_service.get_all_users();
        res.status(200).json(users);
    }))
    .post(userValidateCreateOrPut, raw( async (req: Request, res: Response): Promise<void> => {
        const user = await user_service.create_user(req.body);
        res.status(200).json(user);
    }));


userRouter.route('/:id')
    .get(raw( async (req: Request, res: Response): Promise<void> => {
        const user = await user_service.get_user_by_id(req.params.id);
        if(!user) res.status(404).json({ status: 400, message: `User ${req.params.id} not found.` });
        else res.status(200).json(user);
    }))
    .put(userValidateCreateOrPut, raw( async (req: Request, res: Response): Promise<void> => {
        let updatedUser = await user_service.find_user_by_id_and_update(req.params.id, req.body);
        if(!updatedUser) res.status(404).json({ status: 400, message: `User ${req.params.id} not found.` });
        else res.status(200).json(updatedUser);
    }))
    .patch(userValidatePatch ,raw( async (req: Request, res: Response): Promise<void> => {
        let updatedUser = await user_service.find_user_by_id_and_update(req.params.id, req.body);
        if(!updatedUser) res.status(404).json({ status: 400, message: `User ${req.params.id} not found.` });
        else res.status(200).json(updatedUser);
    }))
    .delete(raw( async (req: Request, res: Response): Promise<void> => {
        let deletedUser = await user_service.find_user_by_id_and_delete(req.params.id);
        if(!deletedUser) res.status(404).json({ status:400, message: `User ${req.params.id} not found.` });
        else res.status(200).json(deletedUser);
    }));

export default userRouter;
//###############################################

// // CREATES A NEW USER
// router.post("/", validateCreateOrPut, raw( async (req: Request, res: Response): Promise<void> => {
//     const user = await user_model.create(req.body);
//     res.status(200).json(user);
// }) );


// // GET ALL USERS
// router.get( "/",raw(async (req: Request, res: Response): Promise<void> => {
//     const users: UserData[] = await user_model.find()
//                                   .select(`_id 
//                                           first_name 
//                                           last_name 
//                                           email 
//                                           phone`);
//     res.status(200).json(users);
//   })  
// );

// // GET USERS - PAGINATION
// router.get( "/:page/:numPerPage",raw(async (req: Request, res: Response): Promise<void> => {
//   const users: UserData[] = await user_model.find()
//                                 .select(`_id 
//                                         first_name 
//                                         last_name 
//                                         email 
//                                         phone`)
//                                 .skip( Number(req.params.page) > 0 ? ( ( Number(req.params.page) - 1 ) * Number(req.params.numPerPage) ) : 0 )
//                                 .limit( Number(req.params.numPerPage) )
//   res.status(200).json(users);
// })  
// );


// // GETS A SINGLE USER
// router.get("/:id",raw(async (req: Request, res: Response): Promise<Response | void> => {
//     const user: UserData = await user_model.findById(req.params.id)
//     if (!user) return res.status(404).json({ status: "No user found." });
//     res.status(200).json(user);
//   })
// );
// // UPDATES A SINGLE USER - PUT
// router.put("/:id", validateCreateOrPut,raw(async (req: Request, res: Response): Promise<void> => {
//   const user: UserData = await user_model.findByIdAndUpdate(req.params.id,req.body, 
//     {new: true, upsert: false });
//   res.status(200).json(user);
//   })
// );

// // UPDATES A SINGLE USER - PATCH
// router.patch("/:id", validatePatch, raw(async (req: Request, res: Response): Promise<void> => {
//   const user: UserData = await user_model.findByIdAndUpdate(req.params.id,req.body, 
//     {new: true, upsert: false });
//   res.status(200).json(user);
//   })
// );


// // DELETES A USER
// router.delete("/:id",raw(async (req: Request, res: Response): Promise<void | Response> => {
//     const user: UserData = await user_model.findByIdAndRemove(req.params.id);
//     if (!user) return res.status(404).json({ status: "No user found." });
//     res.status(200).json(user);
//   })
// );

// export default router;
