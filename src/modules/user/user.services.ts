import { UserData } from '../../typings/interfaces.js';
import adapter from './user.mongo.adapter.js';
import * as artist_service  from '../artist/artist.services.js';
import * as playlist_service  from '../playlist/playlist.services.js';


// GET /api/users/
export const get_all_users = async (): Promise<UserData[]> => {
    const users = await adapter.get_all_users();
    return users;
}

// POST /api/user/
export const create_user = async (payload: UserData): Promise<UserData> => {
    const newUser = await adapter.create_user(payload);
    return newUser;
}

// GET /api/users/:id
export const get_user_by_id = async (user_id: string): Promise<UserData | null> => {
    const user = await adapter.find_user_by_id(user_id);
    return user;
}

// PUT OR PATCH /api/users/:id
export const find_user_by_id_and_update = async (user_id: string, payload: UserData): Promise<UserData | null> => {
    const updatedUser = await adapter.find_user_by_id_and_update(user_id, payload);
    return updatedUser;
}

// DELETE /api/users/:id
export const find_user_by_id_and_delete = async (user_id: string): Promise<UserData | null> => {
    const deletedUser = await adapter.find_user_by_id_and_delete(user_id);
    // Also delete all user's playlists
    if(deletedUser) {        
        deletedUser.playlist_ids.forEach(async playlist_id => {
            await playlist_service.delete_playlist_by_id(playlist_id);
        })
    }   
    return deletedUser;
}