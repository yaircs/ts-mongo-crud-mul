import * as yup from 'yup';


const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

export const createOrPutSchema = yup.object().shape({
    username:  yup.string().min(2).max(15).required(),
    email:  yup.string().email().required(),
    password:  yup.string().min(8).max(20).required(),
    playlist_ids:  yup.array().required()
});

export const patchSchema = yup.object().shape({
    username:  yup.string().min(2).max(15),
    email:  yup.string().email(),
    password:  yup.string().min(8).max(20),
    playlist_ids:  yup.array()
}).test('at-least-one-field', "you must provide at least one field", value =>
!!(value.username || value.email || value.password || value.playlist_ids)
);